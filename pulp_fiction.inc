%include "colon.inc"

section .data

colon "directed by", directedBy
db "Quentin Tarantino", 0

colon "slogan", slogan
db "Just because you are a character doesn't mean you have character", 0

colon "country", country
db "USA", 0

colon "year", year
db "1994", 0

colon "age", age
db "18+", 0

colon "rating", rating
db "9.2", 0
